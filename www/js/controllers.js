var app = angular.module('caffeinehit.controllers', []);

app.controller("YelpController", function ($scope, YelpService, $http, NgMap) {
	
	
//	$http.get("https://api.jcdecaux.com/vls/v1/stations?contract=dublin&apiKey=923b9f8762f3bacf246b898bf50e66550436b145").success(function (data) {
    
   
  //  $scope.dublinBikes = data;
    
 // });
	
	
  NgMap.getMap().then(function(map) {
    $scope.map = map;
  });
	
	$scope.showCafeDetail = function (event,cafe) {
		
		
		alert('showDetail');
		
		
	}
	
	
	$scope.cities = [
    {id: 1,name: 'Oslo', pos:[59.923043, 10.752839]},
    {id: 2,name: 'Stockholm',pos:[59.339025, 18.065818]},
    {id: 3,name: 'Copenhagen',pos:[55.675507, 12.574227]},
    {id: 4,name: 'Berlin',pos:[52.521248, 13.399038]},
    {id: 5,name: 'Paris',pos:[48.856127, 2.346525]}
  ];
  
  $scope.showCity = function(event, cafe) {
  //  $scope.selectedCity = city;
  
 // console.log(cafe);
  $scope.yelp.cafe = cafe;
  
    $scope.map.showInfoWindow('marker-info', this);
  };
	
	
	
	$scope.yelp = YelpService;

	$scope.doRefresh = function () {
		if (!$scope.yelp.isLoading) {
			$scope.yelp.refresh().then(function () {
				$scope.$broadcast('scroll.refreshComplete');
			});
		}
	};

	$scope.loadMore = function () {
		console.log("loadMore");
		if (!$scope.yelp.isLoading && $scope.yelp.hasMore) {
			$scope.yelp.next().then(function () {
				$scope.$broadcast('scroll.infiniteScrollComplete');
			});
		}
	};

	$scope.getDirections = function (cafe) {
		console.log("Getting directions for cafe");
		var destination = [
			cafe.location.coordinate.latitude,
			cafe.location.coordinate.longitude
		];

		var source = [
			$scope.yelp.lat,
			$scope.yelp.lon
		];

		launchnavigator.navigate(destination, source);
	};
});
